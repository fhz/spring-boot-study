package com.example.fhz.a.annotation;

import com.example.fhz.a.constants.Constants;

import java.lang.annotation.*;

@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface LikeAnnon {


    String value() default Constants.LIKE_CONSTANTS.ALL;

}
