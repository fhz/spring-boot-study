package com.example.fhz.a.utils;

import lombok.experimental.UtilityClass;
import org.springframework.util.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Classname StringUtils
 * @Description 通用String工具类
 * @Date 2021/2/26 9:18
 * @Created by fisher.fang
 */
@UtilityClass
public class LcfcStringUtils {

    private static Pattern humpPattern = Pattern.compile("[A-Z]");
    /**
     * 大写转下划线
     *
     * @param str
     * @return
     */
    public static String humpToLine(String str) {
        Matcher matcher = humpPattern.matcher(str);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sb, "_" + matcher.group(0).toLowerCase());
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    /**
     * 下划线转驼峰
     *
     * @param filed 对应字符串
     */
    public static String toCamelCase(String filed) {
        String split = "_";
        String replace = "";
        StringBuilder newString = new StringBuilder();
        int first;
        while (filed.contains(split)) {
            first = filed.indexOf(split);
//            System.out.println("分隔符位置：" + first);
            if (first != filed.length()) {
                newString.append(filed, 0, first).append(replace);
//                System.out.println("当前newString拼接为：" + newString);
                filed = filed.substring(first + split.length());
                filed = firstCharacterToUpper(filed);
            }
        }
        newString.append(filed);
        return newString.toString();
    }

    /**
     * 下划线转驼峰
     * @param column
     * @return
     */
    public static String convertToCamelCase(String column) {
        return convertToCamelCase(column,false);
    }

    /**
     * 下划线转驼峰
     * @param column
     * @param first 首字母大写
     * @return
     */
    public static String convertToCamelCase(String column,Boolean first) {
        if (first){
            return firstCharacterToUpper(toCamelCase(column));
        }else return toCamelCase(column);
    }

    /**
     * 首字母大写
     *
     * @param filed 对应字符串
     */
    private static String firstCharacterToUpper(String filed) {
        return filed.substring(0, 1).toUpperCase() + filed.substring(1);
    }


    /**
     * 字符串插入到目标字符指定字符前面
     * @param target 目标支付
     * @param addStr 待插入支付
     * @param assign 目标字符中指定字符
     * @return
     */
    public String addStrToIndex(String target,String addStr,String assign){
        String[] splitStr = target.split(assign);

        StringBuilder sql = new StringBuilder();

        if (splitStr.length==2){

            sql.append(splitStr[0]).append(" "+addStr).append(" "+assign+" "+splitStr[1]);

        }else sql.append(target).append(" "+addStr);
        return sql.toString();
    }

    /**
     * 字符串对比
     * @param target  目标字符
     * @param reg 正则
     * @return
     */
    public static Boolean mathStr(String target,String reg){
        String[] targetStr = target.split("/");
        String[] originStr = reg.split("/");

        if (targetStr.length>=originStr.length){
            for (int i = 0; i < originStr.length; i++) {
                String itemOri = originStr[i].trim();
                String itemTar = targetStr[i].trim();
                if (!StringUtils.pathEquals(itemOri,itemTar)){
                    if (itemOri.equals("**")){
                        return true;
                    }
                    return false;
                }
                if (i== originStr.length-1){
                    return true;
                }
            }
        }else if (targetStr.length<originStr.length){
            for (int i = 0; i < targetStr.length; i++) {
                String itemOri = originStr[i].trim();
                String itemTar = targetStr[i].trim();
                if (!StringUtils.pathEquals(itemOri,itemTar)){
                    if (itemOri.equals("**")){
                        return true;
                    }
                    return false;
                }
                if (i== targetStr.length-1){
                    return true;
                }
            }
        }
        return false;
    }


    public static void main(String[] args) {
        System.out.println( mathStr("/actuor/123","/actuor/**"));;

//        System.out.println("health".matches(".*"));

//        String target = "SELECT id,type_id,name,value,sort,create_by,create_time,update_by,update_time,remark " +
//                "FROM sys_permission_value LIMIT ? ";
//
//        String addStr  = " where 1=1 and create_by='qiuxue.wu' or create_by='admin'";
//
//        String assign = "LIMIT".toUpperCase();
//        System.out.println(addStrToIndex(target,addStr,assign));


    }

}
