package com.example.fhz.a.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * @author fisher.fang
 * @description:
 * @date 2021/8/17/017 16:00
 */
@Component
@Aspect
public class UpdateOrSaveAop {

    @Pointcut(
            "execution(* com.example.fhz.a.controller.*.save*(..))||" +
            "execution(* com.example.fhz.a.controller.*.update*(..))"
    )
    public void updatePointcut(){

    }

    @Pointcut("execution(* com.example.fhz.a.controller.*.*getById(..))")
    public void seletePointcut(){

    }


    @AfterReturning(pointcut = "seletePointcut()",returning = "jsonResult")
    public void selectAfterreturn(JoinPoint point,Object jsonResult){

        System.out.println("select");

        System.out.println(jsonResult);

    }



    @AfterReturning(pointcut = "updatePointcut()",returning = "jsonResult")
    public void afterreturn(JoinPoint point,Object jsonResult){

        System.out.println("add or update ");

        System.out.println(jsonResult);

    }


}
