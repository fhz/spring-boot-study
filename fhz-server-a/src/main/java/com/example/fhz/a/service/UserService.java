package com.example.fhz.a.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.fhz.a.entity.SysUser;

/**
 * @author fisher.fang
 * @description:
 * @date 2021/8/17/017 15:24
 */
public interface UserService extends IService<SysUser> {
}
