package com.example.fhz.a.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum OrderType {
    DESC(1),ASC(2)
    ;
    private Integer code;

}
