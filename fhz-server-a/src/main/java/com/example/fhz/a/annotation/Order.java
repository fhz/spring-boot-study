package com.example.fhz.a.annotation;

import com.example.fhz.a.constants.OrderType;

import java.lang.annotation.*;

@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface Order {



    OrderType type() default OrderType.ASC;

}
