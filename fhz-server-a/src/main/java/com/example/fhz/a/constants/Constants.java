package com.example.fhz.a.constants;

import java.util.concurrent.TimeUnit;

public interface Constants {

    String UTF8 = "UTF-8";

    //最大存储长度
    Integer STORE_LENGTH = 3000;

    /**
     * 验证码 redis key
     */
    String CAPTCHA_CODE_KEY = "captcha_codes:";

    String USER_AGENT = "User-Agent";

    String HTTP_CODE = "code";

    String SERIALVERSIONUID = "serialVersionUID";

    String SUCCESS_OPT = "success.opt";

    String ERROR_OPT = "error.opt";

    /**
     * 验证码有效期（分钟）
     */
    public static final long CAPTCHA_EXPIRATION = 2;

    public static final long CAPTCHA_FREQUENCY_TIME = 5;
    public static final int MAX_CAPTCHA_FREQUENCY = 10;

    /**
     * 用户ID字段
     */
    public static final String DETAILS_USER_ID = "user_id";

    String USER_EXTEND = "user_extend";

    String LOGIN_ERROR_MSG = "用户名或者密码错误";

    /**
     * 配置环境 app.id
     */
    String APP_ID = "app.id";

    /**
     * 默认应用id
     */
    Long DEFAULT_APP_ID=-1L;

    /**
     * 用户所属appid字段
     */
    String APP_ID_Field = "userAppIds";
    /**
     * 用户超管标识字段
     */
    String SUPER_ADMIN_FIELD = "superAdmin";

    String DEPT_FIELD = "deptId";

    String CREATE_BY = "createBy";

    String ANONYMOUS_USER_ID="55555XXX";

    /**
     * 用户名字段
     */
    public static final String DETAILS_USERNAME = "username";


    /**
     * 授权字段标识
     */
    static final String AUTHORIZATION_HEADER = "authorization";

    /**
     * 令牌前缀
     */
     String TOKEN_PREFIX = "Bearer ";
    /**
     * 令牌自定义标识
     */
    String HEADER = "Authorization";

    /**
     * oauth2 前缀
     */
    String OAUTH2_PREFIX = "oauth:token:";

    /**
     * oauth 客户端信息
     */
    String CLIENT_DETAILS_KEY = "lcfc:client:details";

    /**
     * 在线用户信息缓存key前缀
     */
    String LCFC_ONLINE_KEy = "lcfc:online:";

    /**
     * 在线用户token key
     */
    String LCFC_ONLINE_TOKEN = "lcfc:onlineToken:";

    /**
     * 平台内系统用户的唯一标志
     */
    public static final String SYS_USER = "SYS_USER";

    /** 正常状态 */
    public static final String NORMAL = "0";

    /** 异常状态 */
    public static final String EXCEPTION = "1";

    /** 用户封禁状态 */
    public static final String USER_DISABLE = "1";

    /** 角色封禁状态 */
    public static final String ROLE_DISABLE = "1";

    String SUPER_ADMIN="1";

    /** 部门正常状态 */
    public static final String DEPT_NORMAL = "0";

    /** 部门停用状态 */
    public static final String DEPT_DISABLE = "1";

    /** 字典正常状态 */
    public static final String DICT_NORMAL = "0";

    /** 是否为系统默认（是） */
    public static final String YES = "Y";

    /** 是否菜单外链（是） */
    public static final String YES_FRAME = "0";

    /** 是否菜单外链（否） */
    public static final String NO_FRAME = "1";


    /** Layout组件标识 */
    public final static String LAYOUT = "Layout";

    /** ParentView组件标识 */
    public final static String PARENT_VIEW = "ParentView";

    /** 校验返回结果码 */
    public final static String UNIQUE = "0";

    public final static String NOT_UNIQUE = "1";

    Integer NORMAL_CODE = 200;

    /** 后台请求 */
    String backRequest = "1";


    /**
     * 字典管理 cache key
     */
    String SYS_DICT_KEY = "sys_dict:";

    //国际化缓存键
    String I18N_KEY = "i18n:";

    String I18N_VALUE= "i18nValue";

    String DEFAULT_I18N = "zh-cn";

    String HTTP = "http://";

    String HTTPS="https://";

    String STRING_SPLIT = ",";


    /**
     * 用户操作权限缓存前缀
     */
    String SYS_MENUINIT_KEY = "permission:menu:";

    /**
     * 用户部门权限信息
     */
    String SYS_USER_DEPT = "permission:dept:";

    /**
     * 用户权限数据缓存前缀
     */
    String USER_PERMISSION_PREF="permission:user:";

    /**
     * 应用包含用户缓存键
     */
    String APP_HAS_USER_PRE = "permission:apphasUser";

    String USER_APP_KEY = "permission:app:";

    /**
     * validate code prefix
     */
    String VALIDATE_CODE = "lcfc:code:";

    /**
     * 事件缓存前缀
     */
    String EVENT_OREF = "event:";



    interface LOCK_INFO{
        /**
         * 缓存锁前缀
         */
        String LOCK_OREF = "lock:";

        Integer LOCK_VALUE = 1;

        long TIMEOUT = 5*60;

        TimeUnit TIME_UNIT = TimeUnit.SECONDS;

    }

    interface LIKE_CONSTANTS{
        String ALL = "all";

        String RIGHT = "right";

        String LEFT = "left";
    }

}
