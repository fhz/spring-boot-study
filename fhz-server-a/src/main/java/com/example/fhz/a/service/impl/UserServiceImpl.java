package com.example.fhz.a.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.fhz.a.entity.SysUser;
import com.example.fhz.a.mapper.UserMapper;
import com.example.fhz.a.service.UserService;
import org.springframework.stereotype.Service;

/**
 * @author fisher.fang
 * @description:
 * @date 2021/8/17/017 15:25
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, SysUser> implements UserService {
}
