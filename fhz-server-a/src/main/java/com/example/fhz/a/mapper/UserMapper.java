package com.example.fhz.a.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.fhz.a.entity.SysUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author fisher.fang
 * @description:
 * @date 2021/8/17/017 15:24
 */
@Mapper
public interface UserMapper extends BaseMapper<SysUser> {
}
