package com.example.fhz.a.utils;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.fhz.a.annotation.LikeAnnon;
import com.example.fhz.a.annotation.Order;
import com.example.fhz.a.constants.Constants;
import com.example.fhz.a.constants.OrderType;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.util.*;

/**
 * @Classname BeanUtils
 * @Description TODO
 * @Date 2021/1/28 14:01
 * @Created by fisher.fang
 */
public class BeanUtils {



    /**
     * @param t               实体类
     * @param inParams        in查询条件
     * @param <T>
     * @return
     */
    public static <T> QueryWrapper<T> createQueryWrapper(Object t, Map<String, List> inParams) {
        Class<?> clazz = t.getClass();

        Field[] fields = getAllFields(clazz);

        String superAdmin = "0";

        //获取超管字段数据
        Field superField = null;
        try {

            Optional<Field> optional = Arrays.stream(fields)
                    .filter(x -> Constants.SUPER_ADMIN_FIELD.equalsIgnoreCase(x.getName()))
                    .findFirst();
            if (optional.isPresent()) {
                superField = optional.get();
            }
            if (superField != null) {
                superField.setAccessible(true);

                superAdmin = (String) superField.get(t);

                superField.setAccessible(false);
            }

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        final String isSuper = superAdmin;


        QueryWrapper<T> queryWrapper = new QueryWrapper<T>();

        Arrays.stream(fields).forEach(x -> {
            try {
                x.setAccessible(true);

                //如果不存在该字段，过滤
                TableField tableField = x.getAnnotation(TableField.class);

                if (tableField != null && !tableField.exist()) {
                    return;
                }

                Object fieldValue = x.get(t);
                String fieldName = x.getName();
                Order orderAnn = x.getAnnotation(Order.class);
                LikeAnnon likeAnnon = x.getAnnotation(LikeAnnon.class);

                //排序字段加入
                if (orderAnn != null) {
                    if (OrderType.DESC.getCode().intValue() == orderAnn.type().getCode().intValue()) {
                        queryWrapper.orderByDesc(LcfcStringUtils.humpToLine(fieldName));
                    } else queryWrapper.orderByAsc(LcfcStringUtils.humpToLine(fieldName));
                }
                // 超级管理员 只会进行查询条件
                if (Constants.SUPER_ADMIN.equalsIgnoreCase(isSuper)) {
                    addQueryWrapper(likeAnnon,fieldName, fieldValue, inParams, queryWrapper);

                } else {//以下都是非超管查询
                    // 如果是应用所属查询 且 值为空
                    addQueryWrapper(likeAnnon,fieldName, fieldValue, inParams, queryWrapper);


                }

            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            x.setAccessible(false);
        });
        return queryWrapper;
    }

    /**
     * 增加查询条件
     *
     * @param fieldName    属性名称
     * @param fieldValue   属性值
     * @param inParams     in查询条件
     * @param queryWrapper 查询器
     */
    private static void addQueryWrapper(String fieldName, Object fieldValue
            , Map<String, List> inParams, QueryWrapper queryWrapper) {
        addQueryWrapper(null,fieldName,fieldValue,inParams,queryWrapper);
    }

    /**
     *
     * @param likeAnnon like 注释
     * @param fieldName 属性名称
     * @param fieldValue 属性值
     * @param inParams in查询条件
     * @param queryWrapper 查询器
     */
    private static void addQueryWrapper(LikeAnnon likeAnnon,String fieldName, Object fieldValue
            , Map<String, List> inParams, QueryWrapper queryWrapper) {
        // 值非空 非map类型 非serialVersionUID
        if (!StringUtils.isEmpty(fieldValue) && !(fieldValue instanceof Map) &&
                !fieldName.equalsIgnoreCase(Constants.SERIALVERSIONUID)) {
            // like查询
            if (likeAnnon!=null){
                switch (likeAnnon.value()){
                    case Constants.LIKE_CONSTANTS.LEFT:
                        queryWrapper.likeLeft(LcfcStringUtils.humpToLine(fieldName),fieldValue);break;
                    case Constants.LIKE_CONSTANTS.RIGHT:
                        queryWrapper.likeRight(LcfcStringUtils.humpToLine(fieldName),fieldValue);break;
                    default:
                        queryWrapper.like(LcfcStringUtils.humpToLine(fieldName),fieldValue);
                }
            }else {
                queryWrapper.eq(LcfcStringUtils.humpToLine(fieldName), fieldValue);
            }
        } else if (StringUtils.isEmpty(fieldValue)
                && inParams != null && inParams.containsKey(fieldName)) {
            //如果是in查询 且属性没有设置，in查询为准
            queryWrapper.in(LcfcStringUtils.humpToLine(fieldName),
                    inParams.get(fieldName).toArray());
        }
    }



    /**
     * 根据实体类 创建条件查询语句
     *
     * @param t
     * @param <T>
     * @return
     */
    public static <T> QueryWrapper<T> createQueryWrapper(Object t) {
        return createQueryWrapper(t, null);

    }





    /**
     * 获取本类及其父类的属性的方法
     *
     * @param clazz 当前类对象
     * @return 字段数组
     */
    public static Field[] getAllFields(Class<?> clazz) {
        List<Field> fieldList = new ArrayList<>();
        while (clazz != null) {
            fieldList.addAll(new ArrayList<>(Arrays.asList(clazz.getDeclaredFields())));
            clazz = clazz.getSuperclass();
        }
        Field[] fields = new Field[fieldList.size()];
        return fieldList.toArray(fields);
    }

    /**
     * 获取实体类中某个属性
     *
     * @param clazz
     * @param fieldName
     * @return
     */
    public static Field getOneFieldByClass(Class clazz, String fieldName) {
        return Arrays.stream(getAllFields(clazz))
                .filter(x -> fieldName.equalsIgnoreCase(x.getName())).findFirst().get();

    }



}
