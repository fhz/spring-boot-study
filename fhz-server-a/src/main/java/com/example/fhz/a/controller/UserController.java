package com.example.fhz.a.controller;

import com.example.fhz.a.entity.SysUser;
import com.example.fhz.a.service.UserService;
import com.example.fhz.a.utils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author fisher.fang
 * @description:
 * @date 2021/8/17/017 15:25
 */
@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("all")
    public Object getAll(){
        return userService.list();
    }

    @GetMapping("getById")
//    @Cacheable(cacheNames = "lcfc:user",key = "#id")
    public SysUser getById(String id){
        return userService.getById(id);
    }

    @PostMapping("save")
    public Object save(SysUser user){
         boolean save = userService.save(user);
         if (save){
             return userService.getOne(BeanUtils.createQueryWrapper(user));
         }else return null;
    }

    @PostMapping("update")
    public Object update(SysUser user){
        Boolean updata =  userService.updateById(user);
        if (updata){
            return userService.getById(user.getId());

        }else return null;
    }



}
